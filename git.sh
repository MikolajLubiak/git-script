#!/usr/bin/env sh

git init
git remote add origin git@git.disroot.org:MikolajLubiak/$1.git
git pull origin master
git add -A
git commit -m "$2"
git push -f origin master
